#!/bin/bash
set -e

mysql_log() {
	local type="$1"; shift
	printf '%s [%s] [Entrypoint]: %s\n' "$(date --rfc-3339=seconds)" "$type" "$*"
}

mysql_note() {
	mysql_log Note "$@"
}

docker_change_passw() {
echo "Creo la base de datos si no existe ${MYSQL_DATABSE}"
echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}" | mysql -u root
echo "Reseteo la configuracion de usuario ${MYSQL_USER}"
echo  "CREATE USER IF NOT EXISTS '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}' ; GRANT ALL ON ${MYSQL_DATABASE}.* TO '${MYSQL_USER}'@'%' WITH GRANT OPTION ;"  | mysql -u root

}

docker_temp_server_start() {
 echo "Arranco temporalmente MariaDB para crear la base de datos ${MYSQL_DATABASE} y el usuario ${MYSQL_USER}"
 mysqld -u root &
 PID=$!

 # wait for the db to be ready
 while ! echo "select 1;" |mysql -u root; do
    sleep 1
 done
 docker_change_passw

 kill $PID
 wait $PID
}

mysql_install_db --skip-test-db --skip-log-bin --auth-root-authentication-method=normal

docker_temp_server_start

echo "Iniciando el servidor"
exec "$@"
