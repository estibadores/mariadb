---
title: MariaDB Generic

---

Imagen de MariaDB genérica construida sobre la imagen de GNU/Linux Debian del registry de Sindominio.

Crea:

* Base de datos __mysql__
* Usuaria __mysql__
* Password __mysql__

Estos valores se pueden modificar a través de variables envolventes:

* MYSQL_DATABASE
* MYSQL_USER
* MYSQL_PASSWORD

# Uso en Docker Compose

Configuración en docker-compose.yml

```
...

services:

  db:
    image: registry.sindominio.net/mariadb
    container_name: sd_custom_db
    restart: always
    volumes:
      - dbdata:/var/lib/mysql
    environment:
      MYSQL_DATABASE: customdatabase
      MYSQL_USER: customuser
      MYSQL_PASSWORD: custompassword

...

```
