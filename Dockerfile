FROM registry.sindominio.net/debian

# Default variables
ENV HOME_DIR /var/lib/mysql
ENV MYSQL_DATABASE mysql
ENV MYSQL_USER mysql
ENV MYSQL_PASSWORD mysql

RUN apt-get update && \
    apt-get -qy install mariadb-server default-mysql-client sed &&\
    apt-get clean

COPY 50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf

RUN set -e; \
	mkdir /run/mysqld; \
	chmod 777 /run/mysqld

COPY docker-entrypoint.sh /usr/local/bin/

RUN ["chmod", "+x", "/usr/local/bin/docker-entrypoint.sh"]

VOLUME /var/lib/mysql

EXPOSE 3306

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["mysqld"]
